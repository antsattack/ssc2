﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Diagnostics;

namespace ShopNaSuaCasa.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        public MainPageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Main age";

            var MainPage = new ContentPage()
            {
                // Accomodate iPhone status bar.
                //Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5),
                Content = new ContentView
                {
                    Content = new Label { Text = "Pagina 1" },
                }
            };

            var TestePage = new ContentPage()
            {
                // Accomodate iPhone status bar.
                //Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5),
                Content = new ContentView
                {
                    Content = new Label { Text = "Pagina 2" },
                }
            };

            MyItemsSource = new ObservableCollection<ContentPage>()
            {
                MainPage, TestePage
            };

            MyCommand = new Command(() =>
            {
                Debug.WriteLine("Position selected.");
            });
        }

        ObservableCollection<ContentPage> _myItemsSource;
        public ObservableCollection<ContentPage> MyItemsSource
        {
            set
            {
                _myItemsSource = value;
                RaisePropertyChanged("MyItemsSource");
            }
            get
            {
                return _myItemsSource;
            }
        }

        public Command MyCommand { protected set; get; }


    }
}
